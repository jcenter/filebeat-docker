FROM alpine:3.10

MAINTAINER wangkun23 <845885222@qq.com>

ENV VERSION=7.8.1

WORKDIR /usr/share/filebeat

RUN apk add --update-cache curl bash libc6-compat && \
    rm -rf /var/cache/apk/* && \
    curl https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-${VERSION}-linux-x86_64.tar.gz -o filebeat.tar.gz && \
    tar xzvf filebeat.tar.gz && \
    rm filebeat.tar.gz && \
    mv filebeat-${VERSION}-linux-x86_64/* /usr/share/filebeat/

VOLUME /usr/share/filebeat/data

CMD ["./filebeat","-e"]
